package time;


import org.junit.Test;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

public class TimeTest {

    /**
     * https://docs.oracle.com/javase/8/docs/api/java/time/format/DateTimeFormatter.html
     */
    @Test
    public void timeTest() {
        Date date = new Date();

        long unix = date.getTime();
        System.out.println("UNIX: " + unix);

        Instant instant = date.toInstant();

        String iso = DateTimeFormatter.ISO_OFFSET_DATE_TIME
                .withZone(ZoneId.systemDefault()).format(instant);
        System.out.println("ISO: " + iso);

        String rfc = DateTimeFormatter.RFC_1123_DATE_TIME
                .withZone(ZoneId.systemDefault()).format(instant);
        System.out.println("RFC: " + rfc);
    }

}
