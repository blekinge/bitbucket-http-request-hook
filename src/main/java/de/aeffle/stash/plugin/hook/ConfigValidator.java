package de.aeffle.stash.plugin.hook;

import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.scope.Scope;
import com.atlassian.bitbucket.setting.Settings;
import com.atlassian.bitbucket.setting.SettingsValidationErrors;
import com.atlassian.bitbucket.setting.SettingsValidator;
import de.aeffle.stash.plugin.hook.validators.*;

import javax.annotation.Nonnull;
import java.util.List;


public class ConfigValidator  implements SettingsValidator {

    @Override
    public void validate(@Nonnull Settings settings,
                         @Nonnull SettingsValidationErrors errors,
                         @Nonnull Scope scope) {

        LocationCountValidator locationCountValidator = new LocationCountValidator(settings);
        if (checkForErrors(errors, locationCountValidator)) return;

        UrlValidator urlValidator = new UrlValidator(settings);
        if (checkForErrors(errors, urlValidator)) return;

        FilterValidator filterValidator = new FilterValidator(settings);
        if (checkForErrors(errors, filterValidator)) return;
    }


    private boolean checkForErrors(SettingsValidationErrors errors, Validator validator) {
        if (validator.hasErrors()) {
            List<ValidationError> locationCountValidatorErrors = validator.getErrors();

            for (ValidationError error : locationCountValidatorErrors) {
                errors.addFieldError(error.getKey(), error.getValue());
            }
            return true;
        }
        return false;
    }

}
