package regex;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;


public class RegexTest {

    @Test
    public void regexTest1A() {
        boolean test = "admin".matches("(?!jenkins).*");
        assertThat(test).isTrue();
    }

    @Test
    public void regexTest1B() {
        boolean test = "admin".matches("^(admin)$");
        assertThat(test).isTrue();
    }

    @Test
    public void regexTest2A() {
        boolean test = "admin".matches("admin");
        assertThat(test).isTrue();
    }

    @Test
    public void regexTest2B() {
        boolean test = "admin".matches("^admin$");
        assertThat(test).isTrue();
    }


    @Test
    public void regexTest3A() {
        boolean test = "admin".matches("^(?!jenkins).*$");
        assertThat(test).isTrue();
    }

    @Test
    public void regexTest3B() {
        boolean test = "jenkins".matches("^(?!jenkins).*$");
        assertThat(test).isFalse();
    }
}
