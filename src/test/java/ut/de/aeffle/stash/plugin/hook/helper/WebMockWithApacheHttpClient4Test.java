package ut.de.aeffle.stash.plugin.hook.helper;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import lombok.extern.log4j.Log4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.fluent.Content;
import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.*;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.util.EntityUtils;
import org.junit.Rule;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.assertj.core.api.Assertions.assertThat;


@Log4j
public class WebMockWithApacheHttpClient4Test {
    private static final int CONNECTION_TIMEOUT_MILLIS = 500;
    private static final int SOCKET_TIMEOUT_MILLIS = 500;

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(8080);

    @Test
    public void apacheHttpGetTest() throws IOException {
        //GIVEN
        stubFor(get(urlEqualTo("/my"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withBody("Some content")));

        //WHEN
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet("http://localhost:8080/my");

        int status;
        String result;

        try (CloseableHttpResponse response = httpclient.execute(httpGet)) {
            status = response.getStatusLine().getStatusCode();
            HttpEntity entity = response.getEntity();
            InputStream inputStream = entity.getContent();
            result = IOUtils.toString(inputStream);
        }

        //THEN
        verify(1, getRequestedFor(urlEqualTo("/my")));
        assertThat(status).isEqualTo(200);
        assertThat(result).isEqualTo("Some content");
    }


    @Test
    public void apacheHttpSimpleGetTest() throws IOException {
        //GIVEN
        stubFor(get(urlEqualTo("/my"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withBody("Some content")));

        //WHEN
        String url = "http://localhost:8080/my";

        Content content = Request.Get(url)
                                 .execute()
                                 .returnContent();

        //THEN
        verify(1, getRequestedFor(urlEqualTo("/my")));
        assertThat(content.toString()).isEqualTo("Some content");
    }



    @Test
    public void apacheHttpSimpleGet2Test() throws IOException {
        //GIVEN
        stubFor(get(urlEqualTo("/my"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withBody("Some content")));

        //WHEN
        HttpResponse response = Request.Get("http://localhost:8080/my")
                .connectTimeout(CONNECTION_TIMEOUT_MILLIS)
                .socketTimeout(SOCKET_TIMEOUT_MILLIS)
                .execute()
                .returnResponse();

        int status = response.getStatusLine().getStatusCode();

        //TODO: look at this :-)
        byte[] serializedObject = EntityUtils.toByteArray(response.getEntity());

        HttpEntity entity = response.getEntity();
        InputStream inputStream = entity.getContent();
        String content = IOUtils.toString(inputStream);

        //THEN
        verify(1, getRequestedFor(urlEqualTo("/my")));
        assertThat(status).isEqualTo(200);
        assertThat(content).isEqualTo("Some content");
    }

    @Test
    public void getRequestWithBasicAuth() throws IOException {
        //GIVEN
        String user = "foo";
        String pass = "bar";
        stubFor(get(urlEqualTo("/my"))
                .withBasicAuth(user, pass)
                .willReturn(aResponse().withStatus(200)));

        //WHEN
        Request request = Request.Get("http://localhost:8080/my");

        String authString = user + ":" + pass;
        String authStringEnc = new String(Base64.encodeBase64(authString.getBytes()));
        request.addHeader("Authorization", "Basic " + authStringEnc);

        int status =  request.execute()
                             .returnResponse()
                             .getStatusLine()
                             .getStatusCode();

        //THEN
        assertThat(status).isEqualTo(200);
        verify(getRequestedFor(urlEqualTo("/my")));
    }

    @Test
    public void httpGetWithCookies() throws IOException {
        //GIVEN
        stubFor(get(urlEqualTo("/my"))
                .willReturn(aResponse().withStatus(200)));

        //WHEN
        CookieStore cookieStore = new BasicCookieStore();
        BasicClientCookie cookie = new BasicClientCookie("key", "value");
        cookie.setDomain("localhost");
        cookie.setPath("/");
        cookieStore.addCookie(cookie);

        Executor executor = Executor.newInstance();
        int status = executor.use(cookieStore)
                             .execute(Request.Get("http://localhost:8080/my"))
                             .returnResponse()
                             .getStatusLine()
                             .getStatusCode();

        //THEN
        assertThat(status).isEqualTo(200);
        verify(getRequestedFor(urlEqualTo("/my"))
            .withCookie("key", equalTo("value")));
    }


    @Test
    public void httGetRedirectTest() throws IOException {
        //GIVEN
        stubFor(get(urlEqualTo("/api/old-call"))
                .willReturn(temporaryRedirect("/api/new-call")));
        stubFor(get(urlEqualTo("/api/new-call"))
                .willReturn(aResponse().withStatus(200)));

        //WHEN
        CloseableHttpClient client = HttpClientBuilder.create().build();

        Executor.newInstance(client)
                .execute(Request.Get("http://localhost:8080/api/old-call"));

        //THEN
        verify(getRequestedFor(urlEqualTo("/api/old-call")));
        verify(getRequestedFor(urlEqualTo("/api/new-call")));
    }


    @Test
    public void httpPostWithRedirectTest() throws IOException {
        //GIVEN
        stubFor(post(urlEqualTo("/api/old-call"))
                .willReturn(temporaryRedirect("/api/new-call")));
        stubFor(get(urlEqualTo("/api/new-call"))
                .willReturn(aResponse().withStatus(200)));

        //WHEN
        CloseableHttpClient client = HttpClientBuilder.create()
                .setRedirectStrategy(LaxRedirectStrategy.INSTANCE)
                .build();

        Executor.newInstance(client)
                .execute(Request.Post("http://localhost:8080/api/old-call"));

        //THEN
        verify(postRequestedFor(urlEqualTo("/api/old-call")));
        //TODO: Maybe fix it... but not really a problem...
        // verify(getRequestedFor(urlEqualTo("/api/new-call")));
    }
}
